EXECUTE SEM_APIS.DROP_RULEBASE('owlTime_rb');
EXECUTE SEM_APIS.CREATE_RULEBASE('owlTime_rb');
-- ============================================================================
-- Creation de la regle AFTER
-- ============================================================================
PROMPT "Creation de la regle AFTER"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalAfter_rule');

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalAfter_rule',
  '(?x rdf:type :ProperInterval) (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) (?yBegin :inXSDDateTime ?yBeginDateTime)',
   '(yBeginDateTime > xEndDateTime)',
   '(?y :intervalAfter ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#')));

PROMPT "Fin de creation de la regle AFTER"

-- ============================================================================
-- Creation de la regle BEFORE
-- ============================================================================
PROMPT "Creation de la regle BEFORE"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalBefore_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalBefore_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))<0)
    and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))<0)  
    )',
   '(?y :intervalBefore ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle BEFORE"

-- ============================================================================
-- Creation de la regle CONTAINS
-- ============================================================================
PROMPT "Creation de la regle CONTAINS"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalContains_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalContains_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))>0)  
   )',
   '(?y :intervalContains ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle CONTAINS"
-- ============================================================================
-- Creation de la regle DURING
-- ============================================================================
PROMPT "Creation de la regle CONTAINS"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalDuring_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalDuring_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))>0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))<0)  
   )',
   '(?y :intervalDuring ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#')));

PROMPT "Fin de creation de la regle CONTAINS"

-- ============================================================================
-- Creation de la regle EQUALS
-- ============================================================================
PROMPT "Creation de la regle CONTAINS"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalEquals_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalEquals_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))=0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))=0)  
   )',
   '(?y :intervalEquals ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle CONTAINS"

-- ============================================================================
-- Creation de la regle FINISHED_BY
-- ============================================================================
PROMPT "Creation de la regle FINISHED_BY"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalFinishedBy_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalFinishedBy_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))=0)  
   )',
   '(?y :intervalFinishedBy ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle FINISHED_BY"

-- ============================================================================
-- Creation de la regle FINISHES
-- ============================================================================
PROMPT "Creation de la regle FINISHES"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalFinishes_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalFinishes_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))>0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))=0)  
   )',
   '(?y :intervalFinishes ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle FINISHES"

-- ============================================================================
-- Creation de la regle MEETS
-- ============================================================================
PROMPT "Creation de la regle MEETS"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalMeets_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalMeets_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))<0)
    and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))=0)  
    )',
   '(?y :intervalMeets ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle MEETS"

-- ============================================================================
-- Creation de la regle MET_BY
-- ============================================================================
PROMPT "Creation de la regle MET_BY"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalMetBy_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalMetBy_rule',
  '(?x rdf:type :ProperInterval) (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) (?yBegin :inXSDDateTime ?yBeginDateTime)',
   '(yBeginDateTime = xEndDateTime)',
   '(?y :intervalMetBy ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle MET_BY"

-- ============================================================================
-- Creation de la regle OVERLAPPED_BY
-- ============================================================================
PROMPT "Creation de la regle OVERLAPPED_BY"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalOverlappedBy_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalOverlappedBy_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))>0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))>0)  
   )',
   '(?y :intervalOverlappedBy ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle OVERLAPPED_BY"

-- ============================================================================
-- Creation de la regle OVERLAPS
-- ============================================================================
PROMPT "Creation de la regle OVERLAPS"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalOverlaps_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalOverlaps_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))<0)
    and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))>0)  
    )',
   '(?y :intervalOverlaps ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle OVERLAPS"

-- ============================================================================
-- Creation de la regle STARTED_BY
-- ============================================================================
PROMPT "Creation de la regle STARTED_BY"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalStartedBy_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalStartedBy_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))=0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))>0)  
   )',
   '(?y :intervalStartedBy ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#'))); 

PROMPT "Fin de creation de la regle STARTED_BY"

-- ============================================================================
-- Creation de la regle STARTS
-- ============================================================================
PROMPT "Creation de la regle STARTS"

DELETE FROM  mdsys.semr_owlTime_rb
WHERE upper(rule_name) = upper('intervalStarts_rule'); 

INSERT INTO mdsys.semr_owlTime_rb VALUES(
  'intervalStarts_rule',
  '(?x rdf:type :ProperInterval) 
   (?x :hasEnd ?xEnd) 
   (?xEnd :inXSDDateTime ?xEndDateTime) 
   (?x :hasBeginning ?xBegin)
   (?xBegin :inXSDDateTime ?xBeginDateTime) 
   (?y rdf:type :ProperInterval) 
   (?y :hasBeginning ?yBegin) 
   (?yBegin :inXSDDateTime ?yBeginDateTime)
   (?y :hasEnd ?yEnd)
   (?yEnd :inXSDDateTime ?yEndDateTime)', 
  '((timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))<0)  
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xBeginDateTime),
                                 dateTime2TimeStamp(xEndDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))=0)
     and
    (timeIntervalLengthInSeconds(dateTime2TimeStamp(xEndDateTime),
                                 dateTime2TimeStamp(yBeginDateTime))+
     timeIntervalLengthInSeconds(dateTime2TimeStamp(yBeginDateTime),
                                 dateTime2TimeStamp(yEndDateTime))<0)  
   )',
   '(?y :intervalStarts ?x)', 
  SEM_ALIASES(SEM_ALIAS('','http://www.w3.org/2006/time#')));

PROMPT "Fin de creation de la regle STARTS"




