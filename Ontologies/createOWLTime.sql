--DROP TABLE owlTime_data;
CREATE TABLE owlTime_data (id NUMBER, triple SDO_RDF_TRIPLE_S);

--EXECUTE SEM_APIS.DROP_SEM_MODEL('owlTime');
EXECUTE SEM_APIS.CREATE_SEM_MODEL('owlTime', 'owlTime_data', 'triple');