-- ======================================================
-- Fichier : 
-- Auteur  : JM
-- Date    : 18 mai 2009
-- Role    : 
-- ======================================================
--1-should before the DB created
	-- inter the shared file then
		-- . create_dbName.sh
	-- select the data bese we created
		-- . oraenv dbName
	-- connect as sys with oracle password
		-- sqlplus sys/oracle as sysdba
	-- start the db 
		--startup
--2-should we conntect as the user we created for this db
	-- sqlplus userName/userPsw: sqlplus owlst/owlst
--3-should the file catsem run to but the RDF support in our DB
	--1-the file dir: /opt/oracle/product/11g/md/admin
	--2-inter as sys: sqlplus sys/oracle as sysdba
	--3-execute the file: @catsem.sql
--4-should the network is created to have the RDF graph triple
	--EXECUTE SEM_APIS.CREATE_SEM_NETWORK('tbl_space');
--=================================================

--5-DROP TABLE owlSealTrajectory_data;
CREATE TABLE owlSealTrajectory_data (id number, triple SDO_RDF_TRIPLE_S);


--6-EXECUTE SEM_APIS.DROP_SEM_MODEL('owlSealTrajectory');
EXECUTE SEM_APIS.CREATE_SEM_MODEL('owlSealTrajectory', 'owlSealTrajectory_data', 'triple');

--7- create the Seal Rules: "hunting rule"
