DROP TABLE owlOGCSpatial_data;
CREATE TABLE owlOGCSpatial_data (id NUMBER, triple SDO_RDF_TRIPLE_S);

EXECUTE SEM_APIS.DROP_SEM_MODEL('owlOGCSpatial');
EXECUTE SEM_APIS.CREATE_SEM_MODEL('owlOGCSpatial', 'owlOGCSpatial_data', 'triple');