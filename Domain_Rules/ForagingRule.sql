-- ============================================================================
-- Fichier : createOwlSealTrajectory_Rule_Hunting.sql
-- Auteur  : Jamal Malki
-- Date    : Juin 2012
-- Role    : Regle Hunting 
-- ============================================================================
EXECUTE SEM_APIS.DROP_RULEBASE('sealActivities_rb');
EXECUTE SEM_APIS.CREATE_RULEBASE('sealActivities_rb');

-- max_depth > 4 m
-- TAD > 0.9 (0.8 seems to be too low a threshold)
-- Surf_dur < 60 (surface duration = time spent at surface atfer a dive and before the following one. Lower than 60 secondes : 
	--we assume when foraging the seal should dive at high frequencies and not spend too much time at the surface).


-- ============================================================================
-- Creation de la regle AFTER
-- ============================================================================
DELETE FROM  mdsys.semr_sealActivities_rb
WHERE upper(rule_name) = upper('Foraging_rule');

INSERT INTO mdsys.semr_sealActivities_rb VALUES( 
       'Foraging_rule', 
       '(?unDive rdf:type ost:Dive) 
       	(?unDive ost:hasFeature ?uneFeature) 
       	(?uneFeature ost:tad ?unTAD) 
       	(?uneFeature ost:max_depth ?unMaxDepth) 
		(?unDive ost:surf_dur ?unSurfDur)
		(?unDive ost:dive_dur ?unDiveDur)
		(?unDive ost:sequenceHasActivity ?uneActivite)', 
       '(unTAD > 0.9) and (unMaxDepth > 4) and (unSurfDur/unDiveDur < 0.5)',
       '(?uneActivite rdf:type ost:Foraging)', 
       SEM_ALIASES(SEM_ALIAS('ost', 'http://l3i.univ-larochelle.fr/Sido/owlSealTrajectory#')));