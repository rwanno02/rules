EXECUTE SEM_APIS.DROP_RULEBASE('owlSpatialOnto_rb');
EXECUTE SEM_APIS.CREATE_RULEBASE('owlSpatialOnto_rb');

-- ============================================================================
-- Creation of AnyInteract_rule
-- ============================================================================

--delete AnyInteract_rule from rulebase owlSpatialOnto_rb

DELETE FROM  mdsys.semr_owlSpatialOnto_rb
WHERE upper(rule_name) = upper('anyInteract_rule');

INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
 'anyInteract_rule',
 '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''ANYINTERACT'') = 1)',
  '(?spObj1 os:anyInteract ?spObj2)',
  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

-- ============================================================================
-- Creation of Contains_rule
-- ============================================================================
--delete Contains_rule from rulebase owlSpatialOnto_rb

DELETE FROM  mdsys.semr_owlSpatialOnto_rb
WHERE upper(rule_name) = upper('contains_rule');

INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
  'contains_rule',
  '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''CONTAINS'') = 1)',
  '(?spObj1 os:contains ?spObj2)',
  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

-- ============================================================================
-- Creation of CveredBy_rule
-- ============================================================================
--delete CoveredBy_rule from rulebase owlSpatialOnto_rb

DELETE FROM  mdsys.semr_owlSpatialOnto_rb
WHERE upper(rule_name) = upper('coverdBy_rule');

INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
  'coverdBy_rule',
  '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''COVEREDBY'') = 1)',
  '(?spObj1 os:coverdBy ?spObj2)',
  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

-- ============================================================================
-- Creation of Covers_rule
-- ============================================================================
--delete Covers_rule from rulebase owlSpatialOnto_rb

DELETE FROM  mdsys.semr_owlSpatialOnto_rb
WHERE upper(rule_name) = upper('covers_rule');

INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
  'covers_rule',
  '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''COVERS'') = 1)',
  '(?spObj1 os:covers ?spObj2)',
  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

-- ============================================================================
-- Creation of Equals_rule
-- ============================================================================
--delete Equals_rule from rulebase owlSpatialOnto_rb

DELETE FROM  mdsys.semr_owlSpatialOnto_rb
WHERE upper(rule_name) = upper('equal_rule');

INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
  'equal_rule',
  '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''EQUAL'') = 1)',
  '(?spObj1 os:equals ?spObj2)',
  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

-- ============================================================================
-- Creation of Inside_rule
-- ============================================================================
--delete Inside_rule from rulebase owlSpatialOnto_rb

DELETE FROM  mdsys.semr_owlSpatialOnto_rb
WHERE upper(rule_name) = upper('inside_rule');

INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
  'inside_rule',
  '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''INSIDE'') = 1)',
  '(?spObj1 os:inside ?spObj2)',
  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

-- ============================================================================
-- Creation of on_rule
-- ============================================================================
--delete on_rule from rulebase owlSpatialOnto_rb

--DELETE FROM  mdsys.semr_owlSpatialOnto_rb
--WHERE upper(rule_name) = upper('on_rule');

--INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
--  'on_rule',
--  '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
--	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
--  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''ON'') = 1)',
 -- '(?spObj1 os:on ?spObj2)',
--  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

-- ============================================================================
-- Creation of Overlaps_rule
-- ============================================================================
--delete Overlaps_rule from rulebase owlSpatialOnto_rb

DELETE FROM  mdsys.semr_owlSpatialOnto_rb
WHERE upper(rule_name) = upper('overlaps_rule');

INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
  'overlaps_rule',
  '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''OVERLAPS'') = 1)',
  '(?spObj1 os:overlaps ?spObj2)',
  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

-- ============================================================================
-- Creation of Touch_rule
-- ============================================================================
--delete Touch_rule from rulebase owlSpatialOnto_rb

DELETE FROM  mdsys.semr_owlSpatialOnto_rb
WHERE upper(rule_name) = upper('touch_rule');

INSERT INTO mdsys.semr_owlSpatialOnto_rb VALUES(
  'touch_rule',
  '(?spObj1 rdf:type os:Geometry)(?spObj1 os:srid ?sridSpObj1)(?spObj1 os:wkt ?strSpObj1)
	(?spObj2 rdf:type os:Geometry)(?spObj2 os:srid ?sridSpObj2)(?spObj2 os:wkt ?strSpObj2)',
  '(evalSpatialRelation(spObj1,strSpObj1,spObj2,strSpObj2,sridSpObj2,''TOUCH'') = 1)',
  '(?spObj1 os:touch ?spObj2)',
  SEM_ALIASES(SEM_ALIAS('os','http://l3i.univ-larochelle.fr/Sido/owlOGCSpatial#'))); 

--------------------------------------------------------------------------------------

PROMPT "The end of creation"