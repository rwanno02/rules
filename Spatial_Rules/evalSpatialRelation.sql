CREATE OR REPLACE FUNCTION evalSpatialRelation(
								spObj1 VARCHAR2, wkt1 VARCHAR2,
								spObj2 VARCHAR2,  wkt2 VARCHAR2,SRID VARCHAR2,
								relation VARCHAR2)
   RETURN NUMBER IS
   resultRelation VARCHAR2(32);
   relationName VARCHAR2(32);
   res NUMBER;  
   SRIDvalue NUMBER;
 -- FicOUT	UTL_FILE.FILE_TYPE ;
BEGIN
--FicOUT := UTL_FILE.FOPEN ('FICHIERS_OUT','evalSpatialRelation.out' ,'A' ) ;
--UTL_FILE.PUT_LINE(FicOUT,'enter to function ...', true);
--select ID
delete FROM SpatialObj1;
SRIDvalue := to_number(SRID);
--spObj1
INSERT INTO spa.SpatialObj1 VALUES(1, 'obj1', SDO_GEOMETRY(wkt1, SRIDvalue));
--spObj2
INSERT INTO spa.SpatialObj1 VALUES(2, 'obj2', SDO_GEOMETRY(wkt2, SRIDvalue));

SELECT SDO_WITHIN_DISTANCE(c.shape,  b.shape, 'distance=500 unit=M') INTO resultRelation 
	   FROM SpatialObj1 c, SpatialObj1  b
	   WHERE   c.mkt_id = 1
	   AND     b.mkt_id = 2; 

--UTL_FILE.PUT_LINE(FicOUT,resultRelation, true);	   
--query
IF 	(UPPER(resultRelation) = UPPER('TRUE')) THEN		
	--	UTL_FILE.PUT_LINE(FicOUT,spObj1, true);
	--	UTL_FILE.PUT_LINE(FicOUT,spObj2, true);
	--	UTL_FILE.PUT_LINE(FicOUT,wkt1, true);
	--	UTL_FILE.PUT_LINE(FicOUT,wkt2, true);
	--	UTL_FILE.PUT_LINE(FicOUT,relation, true);
	IF (UPPER(relation) = UPPER('CONTAINS')) THEN 
			SELECT SDO_CONTAINS(c.shape, b.shape) INTO resultRelation
						FROM SpatialObj1 c, SpatialObj1  b
						WHERE c.mkt_id = 1
						AND   b.mkt_id = 2;
		--END IF;
		ELSIF (UPPER(relation) = UPPER('EQUAL')) THEN 
			SELECT SDO_EQUAL(c.shape, b.shape) INTO resultRelation
						FROM SpatialObj1 c, SpatialObj1  b
						WHERE c.mkt_id = 1
						AND   b.mkt_id = 2;
		--END IF;
		ELSIF  (UPPER(relation) = UPPER('INSIDE')) THEN 
			SELECT SDO_INSIDE(c.shape, b.shape) INTO resultRelation
						FROM SpatialObj1 c, SpatialObj1  b
						WHERE c.mkt_id = 1
						AND   b.mkt_id = 2;
		--END IF;
		ELSIF  (UPPER(relation) = UPPER('TOUCH')) THEN 
			SELECT SDO_TOUCH(c.shape, b.shape) INTO resultRelation
						FROM SpatialObj1 c, SpatialObj1  b
						WHERE c.mkt_id = 1
						AND   b.mkt_id = 2;
		--END IF;
		ELSIF  (UPPER(relation) = UPPER('COVERS')) THEN 
			SELECT SDO_COVERS(c.shape, b.shape) INTO resultRelation
						FROM SpatialObj1 c, SpatialObj1  b
						WHERE c.mkt_id = 1
						AND   b.mkt_id = 2;
		--END IF;
		ELSIF  (UPPER(relation) = UPPER('COVEREDBY')) THEN 
			SELECT SDO_COVEREDBY(c.shape, b.shape) INTO resultRelation
						FROM SpatialObj1 c, SpatialObj1  b
						WHERE c.mkt_id = 1
						AND   b.mkt_id = 2;
		--END IF;
		ELSIF  (UPPER(relation) = UPPER('OVERLAPS')) THEN 
			SELECT SDO_OVERLAPS(c.shape, b.shape) INTO resultRelation
						FROM SpatialObj1 c, SpatialObj1  b
						WHERE c.mkt_id = 1
						AND   b.mkt_id = 2;
		--END IF;
		--ELSIF  (UPPER(relation) = UPPER('ON')) THEN 
			--SELECT SDO_ON(c.shape, b.shape) INTO resultRelation
				--		FROM SpatialObj1 c, SpatialObj1  b
					--	WHERE c.mkt_id = 1
						--AND   b.mkt_id = 2;
		--END IF;
		ELSIF  (UPPER(relation) = UPPER('ANYINTERACT')) THEN 
				SELECT SDO_ANYINTERACT(c.shape, b.shape) INTO resultRelation
						FROM SpatialObj1 c, SpatialObj1  b
						WHERE c.mkt_id = 1
						AND   b.mkt_id = 2;
		END IF;
		--IF 	(UPPER(resultRelation) = UPPER('TRUE')) THEN
		--	res := 1;
			--DBMS_OUTPUT.PUT_LINE('pass');
		--END IF;
		--UTL_FILE.PUT_LINE(FicOUT,resultRelation, true);	 
		--IF  (UPPER(relation) = UPPER('ANYINTERACT')) THEN 
			--	IF 	(UPPER(resultRelation) = UPPER('FALSE')) THEN
		--					resultRelation := 'TRUE';
				--ELSE							
				--		resultRelation := 'FALSE';
			--	END IF;
	--	END IF;
--END IF;

IF 	(UPPER(resultRelation) = UPPER('FALSE')) THEN
			res := 0;	
	ELSE
			res := 1;
			--DBMS_OUTPUT.PUT_LINE('pass0');	
END IF;
--commit;
--UTL_FILE.PUT_LINE(FicOUT,'leave the function', true);
--UTL_FILE.FCLOSE(FicOUT); 
RETURN res;
END evalSpatialRelation;
/