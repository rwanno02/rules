-- ============================================================================
-- Creation the index SpatialObj_idx
-- ============================================================================

--index SpatialObj_idx
EXECUTE SEM_APIS.DROP_RULES_INDEX ('owlSpatial_idx');

BEGIN
  SEM_APIS.CREATE_RULES_INDEX(
    'owlSpatial_idx',
    SEM_Models('owlSpatial'),
    SEM_Rulebases('OWLPrime','owlSpatialOnto_rb'));
END;
/