

--Apr�s le drop il faut quitter la session et se reconnecter
EXECUTE sem_apis.drop_entailment('owlSealTrajectory_idx_1');


--Cr�ation d'un index de r�gles pour le sauvegarde des inf�rences des deux models 
BEGIN

 SEM_APIS.CREATE ENTAILMENT('owlTrajectory_idx',
 SEM_MODELS('owlTrajectory','owlTime','owlOGCSpatial'),
 SEM_RULEBASES('OWLPrime','owlTime rb','owlOGCSpatial rb'),
 SEM_APIS.REACH CLOSURE,
 NULL,
 'USER RULES=T');

END;

set echo off;
